import StylishQrCode from "views/StylishQrCode";

var routes = [
  {
    collapse: true,
    path: "/utility",
    name: "Utility",
    state: "openUtility",
    views: [
      {
        path: "/qrcode",
        layout: "/auth",
        name: "QrCode",
        mini: "Qr",
        component: StylishQrCode
      }
    ]
  }
];
export default routes;
