import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import QRCodeStyling from "qr-code-styling";

import logo from "assets/img/tekniqi_logo.jpg";
import PreviewLogo from "assets/img/preview_qrcode.png";

class StylishQrCode extends Component {
  constructor(props) {
    super(props);
    this.canvas = React.createRef();
    this.download = this.download.bind(this);
    this.generateQrCode = this.generateQrCode.bind(this);
    this.handleWidthChange = this.handleWidthChange.bind(this);
    this.handleHeightChange = this.handleHeightChange.bind(this);
    this.handleUrlChange = this.handleUrlChange.bind(this);
    this.handleDotTypeChange = this.handleDotTypeChange.bind(this);
    this.state = {
      width: "100",
      height: "100",
      data: "https://tekniqi.com/blog/continuous-delivery/",
      dotType: "rounded"
    };
    var qrCode;
  }

  download() {
    this.qrCode.download();
  }

  generateQrCode() {
    this.qrCode = new QRCodeStyling({
      width: this.state.width,
      height: this.state.height,
      data: this.state.data,
      image: logo,
      dotsOptions: {
        color: "#008B8B",
        type: this.state.dotType
      },
      backgroundOptions: {
        color: "#ffffff"
      }
    });
    const DOMNode = this.canvas.current;
    DOMNode.innerHTML = "";
    this.qrCode.append(this.canvas.current);
  }

  handleWidthChange(event) {
    this.setState({
      width: event.target.value
    });
  }
  handleHeightChange(event) {
    this.setState({
      height: event.target.value
    });
  }
  handleUrlChange(event) {
    this.setState({
      data: event.target.value
    });
  }
  handleDotTypeChange(event) {
    this.setState({
      dotType: event.target.value
    });
  }

  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={4}>
              <Card
                title="QR Code Generator"
                content={
                  <div>
                    <div className="form-group">
                      <label htmlFor="width">Width</label>
                      <input
                        type="text"
                        className="form-control"
                        id="width"
                        onChange={this.handleWidthChange}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="height">Height</label>
                      <input
                        type="text"
                        className="form-control"
                        id="height"
                        onChange={this.handleHeightChange}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="height">Dot Type</label>
                      <select
                        className="form-control"
                        onChange={this.handleDotTypeChange}
                      >
                        <option value="rounded">Rounded</option>
                        <option value="dots">Dots</option>

                        <option value="square">Square</option>
                      </select>
                    </div>
                    <div className="form-group">
                      <label htmlFor="data">URL</label>
                      <input
                        type="text"
                        className="form-control"
                        id="url"
                        onChange={this.handleUrlChange}
                      />
                    </div>

                    <Button
                      bsStyle="info"
                      pullRight
                      fill
                      type="submit"
                      onClick={this.generateQrCode}
                    >
                      Generate QR Code
                    </Button>
                    <Button
                      bsStyle="info"
                      fill
                      type="submit"
                      onClick={this.download}
                    >
                      Download Code
                    </Button>
                    <div className="clearfix" />
                  </div>
                }
              />
            </Col>
            <Col md={8}>
              <Card
                title="Preview"
                content={
                  <div ref={this.canvas}>
                    <img src={PreviewLogo}></img>
                  </div>
                }
                wizard
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default StylishQrCode;
